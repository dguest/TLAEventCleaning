# Declare the package name:
atlas_subdir( TLAEventCleaning )

# Extra dependencies, based on the build environment:
set( extra_deps )
if( XAOD_STANDALONE )
   set( extra_deps Control/xAODRootAccess )
else()
   set( extra_deps GaudiKernel )
endif()

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Tools/PathResolver
                          ${extra_deps})


# External dependencies:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO Graf Gpad )
find_package( Boost COMPONENTS container iostreams program_options filesystem thread system )

# Not sure if needed...?
add_definitions(-DHAVE_BOOST_IOSTREAMS)


#atlas_add_dictionary( TLAAlgosFullRun2Dict TLAAlgosFullRun2/TLAAlgosFullRun2Dict.h
#   LINK_LIBRARIES TLAAlgosFullRun2Lib )

atlas_add_root_dictionary ( TLAEventCleaningLib TLAEventCleaningDictSource
                            ROOT_HEADERS TLAEventCleaning/*.h Root/LinkDef.h
                            EXTERNAL_PACKAGES ROOT Boost
)


# Libraries in the package:
atlas_add_library( TLAEventCleaningLib
   TLAEventCleaning/*.h Root/*.cxx ${TLAEventCleaningDictSource}
   PUBLIC_HEADERS TLAEventCleaning
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
   #CD: i don't think this is all needed...
   LINK_LIBRARIES ${ROOT_LIBRARIES} ${Boost_LIBRARIES} xAODAnaHelpersLib EventLoop xAODBase xAODRootAccess
                   xAODEventInfo GoodRunsListsLib PileupReweightingLib PATInterfaces
                   PathResolver xAODTau xAODJet xAODMuon xAODEgamma
                   xAODTracking xAODTruth MuonMomentumCorrectionsLib
                   MuonEfficiencyCorrectionsLib MuonSelectorToolsLib JetCalibToolsLib
                   JetSelectorToolsLib AthContainers
                   ElectronPhotonFourMomentumCorrectionLib
                   ElectronEfficiencyCorrectionLib ElectronPhotonSelectorToolsLib
                   IsolationSelectionLib IsolationCorrectionsLib
                   ElectronPhotonShowerShapeFudgeToolLib JetInterface 
                   PhotonEfficiencyCorrectionLib METUtilitiesLib METInterface
                   TauAnalysisToolsLib AsgTools xAODMissingET JetResolutionLib
                   AssociationUtilsLib JetEDM JetUncertaintiesLib
                   JetCPInterfaces xAODBTaggingEfficiencyLib TrigConfxAODLib
                   TrigDecisionToolLib xAODCutFlow JetMomentToolsLib
                   TriggerMatchingToolLib xAODMetaDataCnv xAODMetaData
                   JetJvtEfficiencyLib PMGToolsLib JetSubStructureUtils PathResolver JetTileCorrectionLib ${release_libs}
   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${Boost_LIBRARIES} )


atlas_install_data( data/event-veto-info-run2-UPD4-10 data/event-veto-test-2021 )

if( NOT XAOD_STANDALONE )
   atlas_add_component( TLAEventCleaningLib
      src/components/*.cxx
      LINK_LIBRARIES TLAEventCleaningLib )
endif()


# No executables needed for this package, it's all python fun times

