

FolderToDatatype={
    '/LAR/ElecCalibOfl/OFC/PhysWave/RTM/5samples3bins17phases' : 'PhysOFC',
    '/LAR/ElecCalibOfl/Shape/RTM/5samples3bins17phases'        : 'Shape',
    '/LAR/ElecCalibOnl/MphysOverMcal'                       : 'MphysOverMcal',
    '/LAR/ElecCalibOnl/OFC'                                 : 'PhysOFC',
    '/LAR/ElecCalibOnl/Pedestal'                            : 'Pedestal',
    '/LAR/ElecCalibOnl/Ramp'                                : 'Ramp',
    '/LAR/ElecCalibOnl/Shape'                               : 'Shape',

    '/LAR/ElecCalibOfl/AutoCorrs/AutoCorr'                     : 'AutoCorr',
    '/LAR/ElecCalibOfl/AutoCorrs/PhysicsAutoCorr'              : 'AutoCorr',
    '/LAR/ElecCalibOfl/CaliPulseParams/RTM'                    : 'CalibPulseParams',
    '/LAR/ElecCalibOfl/CaliWaves/CaliWaveXtalkCorr'            : 'CaliWave',
    '/LAR/ElecCalibOfl/CaliWaves/CaliWave'                     : 'CaliWave', 
    '/LAR/ElecCalibOfl/DetCellParams/RTM'                      : 'DetCellParams',
    '/LAR/ElecCalibOfl/MphysOverMcal/RTM'                      : 'MphysOverMcal',
    '/LAR/ElecCalibOfl/OFC/CaliWaveXtalkCorr'                  : 'CaliOFC',
    '/LAR/ElecCalibOfl/OFC/CaliWave'                           : 'CaliOFC',
    '/LAR/ElecCalibOfl/OFC/PhysWave/RTM/5samples1phase'        : 'PhysOFC', 
    '/LAR/ElecCalibOfl/OFC/PhysWave/RTM/5samples3bins17phases' : 'PhysOFC',
    '/LAR/ElecCalibOfl/OFC/PhysWave/RTM/5samples'              : 'PhysOFC',
    '/LAR/ElecCalibOfl/Pedestals/Pedestal'                     : 'Pedestal',
    '/LAR/ElecCalibOfl/PhysWaves/RTM'                          : 'PhysWave', 
    '/LAR/ElecCalibOfl/Ramps/RampLinea'                        : 'Ramp',
    '/LAR/ElecCalibOfl/Shape/RTM/5samples1phase'               : 'Shape',
    '/LAR/ElecCalibOfl/Shape/RTM/5samples3bins17phases'        : 'Shape',
    '/LAR/ElecCalibOfl/Shape/RTM/5samples'                     : 'Shape',




    
##     '/LAR/ElecCalib/CaliWaves/CaliWaveXtalkCorr' : 'CaliWave',
##     '/LAR/ElecCalib/DetCellParams/RTM' : 'DetCellParams',
##     '/LAR/ElecCalib/PhysWaves/RTM' : 'PhysWave',
##     '/LAR/ElecCalib/OFC/PhysWave/RTM/5samples3bins17phases' : 'PhysOFC',
##     '/LAR/ElecCalib/AutoCorr' : 'AutoCorr',
##     '/LAR/ElecCalib/MphysOverMcal/RTM' : 'MphysOverMcal',
##     '/LAR/ElecCalib/Pedestals/Pedestal' : 'Pedestal',
##     '/LAR/ElecCalib/Ramps/RampLinea' : 'Ramp',
##     '/LAR/ElecCalibOnl/OFC' : ' PhysOFC',
##     '/LAR/ElecCalib/CaliWaves/CaliWave' : 'CaliWave',
##     '/LAR/ElecCalib/OFC/CaliWave' : 'CaliOFC',
##     '/LAR/ElecCalib/Shape/RTM/5samples3bins17phases' : 'Shape',
##     '/LAR/ElecCalibOnl/Shape' : 'Shape',
##     '/LAR/ElecCalib/OFC/CaliWaveXtalkCorr' : 'CaliOFC',
##     '/LAR/ElecCalib/CaliPulseParams/RTM' : 'CaliPulseParams'
    }
