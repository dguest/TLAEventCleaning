
from glob import glob
import bz2

eventVetoDir = '../data/event-veto-info-run2-UPD4-10/'
eventVetoTypes = set()
eventVetoFileNames = sorted(glob(eventVetoDir+'*.txt*'))
for i in range(len(eventVetoFileNames)):
    eventVetoFileName = eventVetoFileNames[i]
    runNumber = eventVetoFileName.split('veto-')[-1].split('.')[0]
    print "evaluating", runNumber, '- run', i, 'of', len(eventVetoFileNames)
    if 'bz2' in eventVetoFileName:
        eventVetoFile = bz2.BZ2File(eventVetoFileName)
    else:
        eventVetoFile = open(eventVetoFileName)

    for line in eventVetoFile:
        if not line.startswith('Event'):
            continue
        vetoTypesStr = line.split(']')[0].split('Event Veto ')[-1] + ']'
        vetoTypes = eval(vetoTypesStr)
        # for vetoType in vetoTypes:
            # eventVetoTypes.add(vetoType)
        eventVetoTypes.add(vetoTypesStr)
print eventVetoTypes

