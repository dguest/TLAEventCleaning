
There are various sets of event veto data:
* event-veto-data-2015: from 2015
* event-veto-data-old: 2016 with 2015 tool
* event-veto-data: first attempt at adding miniNoiseBursts for 2016
* event-veto-info-periodF: correct tool for period F
* event-veto-info-all2016: as above, for full year up to 311481 (info-periodF is a subset)
* event-veto-info-all2016-leftovers: 5 runs that didn't finish properly for above
* event-veto-info-merge: the 2015+2016 offering for all runs (a mix of info-all2016, info-all2016-leftovers and data-2015)
* event-veto-info-run2-UPD4-10: the 2015+2016+2017+2018 data for all runs
