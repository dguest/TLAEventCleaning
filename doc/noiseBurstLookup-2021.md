
# 2021

Previous versions of the event-cleaning DB were incompatible with the latest GRLs, so the event-cleaning DB text files were regenerated with the tag LARBadChannelsOflEventVeto-RUN2-UPD4-10

The files were regenerated with a loop over a list of all the run numbers:

source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh 21.0.20
python /afs/cern.ch/user/l/larcalib/LArDBTools/python/showEventVeto.py -r <runnumber> -e <runnumber> -t LARBadChannelsOflEventVeto-RUN2-UPD4-10 

These files were tested on the grid on 2015-2018 data and were placed in the event-veto-test2021 folder in ./data




